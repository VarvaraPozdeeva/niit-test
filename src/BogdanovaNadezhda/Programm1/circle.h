#ifndef CIRCLE_H
#define CIRCLE_H
#include <qmath.h>

using namespace std;

class Circle
{
private:
    double radius, ference, area;
public:
    void setRadius(double r)
    {
        radius=r;
        area=M_PI*r*r;
        ference=2*M_PI*r;
    }

    void setFerence(double f)
    {
        ference=f;
        radius=f/(2*M_PI);
        area=M_PI*(f/(2*M_PI))*(f/(2*M_PI));
    }

    void setArea(double a)
    {
        area=a;
        radius=sqrt(a/M_PI);
        ference=2*M_PI*sqrt(a/M_PI);
    }

    double getRadius()
    {
        return radius;
    }

    double getFerence()
    {
        return ference;
    }

    double getArea()
    {
        return area;
    }
};

#endif // CIRCLE_H
